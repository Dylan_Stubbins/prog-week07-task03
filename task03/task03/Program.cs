﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task03
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculate(5);
        }
        static void Calculate(int a)
        {
            Console.WriteLine($"{a} + {a} = {a + a}");
            Console.WriteLine($"{a} - {a} = {a - a}");
            Console.WriteLine($"{a} / {a} = {a / a}");
            Console.WriteLine($"{a} * {a} = {a * a}");
              

        }
    }
}
